package cz.svetonaut.diruntime.annotation;

import cz.svetonaut.diruntime.interception.DefaultníPřerušovač;
import cz.svetonaut.diruntime.interception.Přerušovač;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Přerušit {

    public Class<? extends Přerušovač> value() default DefaultníPřerušovač.class;
}
