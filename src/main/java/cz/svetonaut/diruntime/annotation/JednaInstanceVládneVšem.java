package cz.svetonaut.diruntime.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ... jedna jim všem káže. Jedna všechny přivede, přes dependency sváže.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface JednaInstanceVládneVšem {
}
