package cz.svetonaut.diruntime;

import cz.svetonaut.diruntime.annotation.HoďMiToSem;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class Container1 {

    public <T> T getInstance(Class<T> clazz) throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        final Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            for (Annotation annotation : field.getDeclaredAnnotations()) {
                if (annotation.annotationType().equals(HoďMiToSem.class)) {
                    field.setAccessible(true);
                    T instance = createInstance(clazz);
                    field.set(instance, this.getInstance(field.getType()));
                    return instance;
                }
            }
        }
        return createInstance(clazz);
    }

    private <T> T createInstance(Class<T> clazz) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        return (T) clazz.getDeclaredConstructor().newInstance();
    }
}
