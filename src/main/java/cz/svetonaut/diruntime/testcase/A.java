package cz.svetonaut.diruntime.testcase;

import cz.svetonaut.diruntime.annotation.HoďMiToSem;

public class A implements I {

    @HoďMiToSem
    private B b;

    @Override
    public String toString() {
        return b.toString();
    }
}
