package cz.svetonaut.diruntime.testcase;

import cz.svetonaut.diruntime.annotation.HoďMiToSem;

public class B implements I{

    @HoďMiToSem
    private C c;

    @Override
    public String toString() {
        return c.toString();
    }
}
