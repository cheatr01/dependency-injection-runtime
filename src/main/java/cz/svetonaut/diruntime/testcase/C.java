package cz.svetonaut.diruntime.testcase;

import cz.svetonaut.diruntime.interception.DefaultníPřerušovač;
import cz.svetonaut.diruntime.annotation.Přerušit;

public class C implements IC{

    private String str = "Obsah třídy C";

    @Override
    public String toString() {
        return str;
    }

    @Přerušit(DefaultníPřerušovač.class)
    @Override
    public String getStr() {
        return str;
    }
}
