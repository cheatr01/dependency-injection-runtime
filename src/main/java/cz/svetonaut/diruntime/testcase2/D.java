package cz.svetonaut.diruntime.testcase2;

import cz.svetonaut.diruntime.annotation.HoďMiToSem;

public class D implements ID {

    @HoďMiToSem
    private IE e;

    @Override
    public String getString() {
        return e.getString();
    }
}
