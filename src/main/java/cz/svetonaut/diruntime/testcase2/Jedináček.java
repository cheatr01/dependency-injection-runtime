package cz.svetonaut.diruntime.testcase2;

import cz.svetonaut.diruntime.annotation.JednaInstanceVládneVšem;

@JednaInstanceVládneVšem
public class Jedináček implements IJedináček {

    private String value = "default";

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Jedináček jedináček = (Jedináček) o;

        return value.equals(jedináček.value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
}
