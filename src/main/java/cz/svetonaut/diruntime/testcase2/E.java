package cz.svetonaut.diruntime.testcase2;

import cz.svetonaut.diruntime.annotation.HoďMiToSem;

public class E implements IE {

    @HoďMiToSem
    private IF f;

    @Override
    public String getString() {
        return f.getString();
    }
}
