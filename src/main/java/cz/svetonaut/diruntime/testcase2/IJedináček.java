package cz.svetonaut.diruntime.testcase2;

public interface IJedináček {
    String getValue();

    void setValue(String value);
}
