package cz.svetonaut.diruntime.handler;

import cz.svetonaut.diruntime.Container2;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public abstract class AbstractInvocationHandler<T> implements InvocationHandler {

    protected final Class<T> instanceClass;
    protected T instance;
    protected final Container2 container;

    public AbstractInvocationHandler(Class<T> instanceClass, Container2 container) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        this.instanceClass = instanceClass;
        this.container = container;
        instance = container.assembleInstance(instanceClass);
    }

    @Override
    public abstract Object invoke(Object proxy, Method method, Object[] args) throws Throwable;

}
