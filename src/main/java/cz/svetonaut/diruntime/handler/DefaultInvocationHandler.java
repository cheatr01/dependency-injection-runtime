package cz.svetonaut.diruntime.handler;

import cz.svetonaut.diruntime.Container2;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class DefaultInvocationHandler<T> extends AbstractInvocationHandler<T> {

    public DefaultInvocationHandler(Class<T> instanceClass, Container2 container) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        super(instanceClass, container);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        final Method instanceMethod = instanceClass.getMethod(method.getName(), method.getParameterTypes());
        return instanceMethod.invoke(instance, args);
    }

}
