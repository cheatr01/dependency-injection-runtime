package cz.svetonaut.diruntime.handler;

import cz.svetonaut.diruntime.Container2;
import cz.svetonaut.diruntime.annotation.Přerušit;
import cz.svetonaut.diruntime.interception.InterceptionProceeder;
import cz.svetonaut.diruntime.interception.InvocationContext;
import cz.svetonaut.diruntime.interception.Přerušovač;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class InterruptibleBeanInvocationHandler<T> extends AbstractInvocationHandler<T> {

    private InterceptionProceeder proceeder;

    public InterruptibleBeanInvocationHandler(Class<T> instanceClass, Container2 container) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        super(instanceClass, container);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        final Method instanceMethod = instanceClass.getMethod(method.getName(), method.getParameterTypes());
        if (instanceMethod.isAnnotationPresent(Přerušit.class)) {
            final Přerušovač interceptor = container.getProxyInstance(instanceMethod.getAnnotation(Přerušit.class).value(), Přerušovač.class);
            this.proceeder = new InterceptionProceeder(interceptor, new InvocationContext(interceptor));
            proceeder.proceed();
        }
        return instanceMethod.invoke(instance, args);
    }
}
