package cz.svetonaut.diruntime.interception;

public class DefaultníPřerušovač implements Přerušovač{

    @Override
    public Object přerušitTímto(InvocationContext obsluha) {
        System.out.println("Přerušeno!");
        return null;
    }
}
