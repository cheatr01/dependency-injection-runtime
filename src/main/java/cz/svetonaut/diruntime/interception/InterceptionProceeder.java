package cz.svetonaut.diruntime.interception;

public class InterceptionProceeder{

    private final Přerušovač přerušovač;
    private final InvocationContext context;

    public InterceptionProceeder(Přerušovač přerušovač, InvocationContext context) {
        this.přerušovač = přerušovač;
        this.context = context;
    }

    public void proceed() {
        přerušovač.přerušitTímto(context);
    }
}
