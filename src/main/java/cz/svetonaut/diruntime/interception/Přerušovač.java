package cz.svetonaut.diruntime.interception;

/**
 * Aka Interceptor
 */
public interface Přerušovač {

    Object přerušitTímto(InvocationContext obsluha);
}
