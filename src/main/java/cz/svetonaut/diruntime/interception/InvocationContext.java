package cz.svetonaut.diruntime.interception;

/**
 * Prepare for next development
 */
public class InvocationContext {

    private Přerušovač přerušovač;

    public InvocationContext(Přerušovač přerušovač){
        this.přerušovač = přerušovač;
    }
}
