package cz.svetonaut.diruntime;

import cz.svetonaut.diruntime.annotation.HoďMiToSem;
import cz.svetonaut.diruntime.annotation.JednaInstanceVládneVšem;
import cz.svetonaut.diruntime.annotation.Přerušit;
import cz.svetonaut.diruntime.handler.AbstractInvocationHandler;
import cz.svetonaut.diruntime.handler.DefaultInvocationHandler;
import cz.svetonaut.diruntime.handler.InterruptibleBeanInvocationHandler;
import cz.svetonaut.diruntime.interception.InterceptionProceeder;
import cz.svetonaut.diruntime.interception.Přerušovač;
import cz.svetonaut.diruntime.testcase2.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class Container2 {

    private static Container2 instance;

    public static Container2 getContainer() {
        if (instance == null) {
            instance = new Container2();
        }
        return instance;
    }

    /**
     * Crutch for "unknown instance class for interface" problem.
     */
    private static final Map<Class<?>, Class<?>> implCol = new HashMap<>();
    static {
        implCol.putIfAbsent(ID.class, D.class);
        implCol.putIfAbsent(IE.class, E.class);
        implCol.putIfAbsent(IF.class, F.class);
    }

    private static final HashMap<Class<?>, Object> singletons = new HashMap<>();

    private Container2() {
    }

    /**
     * first step is create proxy with bean interface and with right invocation handler
     */
    public <T, I> I getProxyInstance(Class<T> clazz, Class<I> face) throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        return (I) Proxy.newProxyInstance(this.getClass().getClassLoader(),
                clazz.getInterfaces(),
                resolveInvocationHandler(clazz));
    }

    /**
     * if any invocation handler need regular instance of injected type. To the new instance is need inject new portion of proxies.
     */
    public <T> T assembleInstance(Class<T> clazz) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        T instance = (T) createInstance(clazz);
        for (Field field : clazz.getDeclaredFields()) {
            for (Annotation annotation : field.getDeclaredAnnotations()) {
                if (annotation.annotationType().equals(HoďMiToSem.class)) {
                    field.setAccessible(true);
                    field.set(instance, getProxyInstance(implCol.get(field.getType()), field.getType()));
                }
            }
        }
        return instance;
    }

    /**
     * If demanded instance is from class annotated singleton like annotation, this method create new instance or return instance created in past call - for other
     * create simply new instance.
     */
    private <T> Object createInstance(Class<T> clazz) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        for (Annotation annotation : clazz.getAnnotations()) {
            if (annotation.annotationType().equals(JednaInstanceVládneVšem.class)) {
                return getOrCreateSingleton(clazz);
            }
        }
        return clazz.getConstructor().newInstance();
    }

    private <T> Object getOrCreateSingleton(Class<T> clazz) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Object instance;
        if (singletons.containsKey(clazz)) {
            instance = singletons.get(clazz);
        }else{
             instance = clazz.getConstructor().newInstance();
            singletons.put(clazz, instance);
        }
        return instance;
    }

    /**
     * for AOP annotated class return AOP invocation handler, for others default invocation handler
     */
    private <T> AbstractInvocationHandler<T> resolveInvocationHandler(Class<T> clazz) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        for (Method declaredMethod : clazz.getDeclaredMethods()) {
            if (declaredMethod.isAnnotationPresent(Přerušit.class)) {
                return new InterruptibleBeanInvocationHandler<T>(clazz, this);
            }
        }
        return new DefaultInvocationHandler<T>(clazz, this);
    }
}
