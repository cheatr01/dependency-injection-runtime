package cz.svetonaut.diruntime;

import cz.svetonaut.diruntime.testcase.A;
import cz.svetonaut.diruntime.testcase.C;
import cz.svetonaut.diruntime.testcase.IC;
import cz.svetonaut.diruntime.testcase2.D;
import cz.svetonaut.diruntime.testcase2.ID;
import cz.svetonaut.diruntime.testcase2.IJedináček;
import cz.svetonaut.diruntime.testcase2.Jedináček;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class DiRuntime {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {

        /*
        First container demonstrate dependency injection by reflection without Aspects possibilities.
        */
        final Container1 container1 = new Container1();
        final A a = container1.getInstance(A.class);
        System.out.println("Container1: " + a.toString());

        /*
        Demonstration of very base usage of dynamic proxies.
        */
        final IC o = (IC) Proxy.newProxyInstance(C.class.getClassLoader(), new Class[]{IC.class}, new InvocationHandler(){
            @Override
            public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
                return "string from invocation handler";
            }
        });
        System.out.println("Ukázková proxy: " + o.getStr());

        /*
        Second container is much more advanced. It uses dynamic proxies for controlling of creating instances and
        for AOP implementation.
         */
        final Container2 container2 = Container2.getContainer();

        /*
        Simple test of injection.
         */
        final ID proxy = container2.getProxyInstance(D.class, ID.class);
        System.out.println("Container2: " + proxy.getString());

        /*
        Test for limited creation of Jedináček class (Singleton).
         */
        final IJedináček prvnáček = container2.getProxyInstance(Jedináček.class, IJedináček.class);
        final IJedináček druháček = container2.getProxyInstance(Jedináček.class, IJedináček.class);
        System.out.println("Prvnáček default: " + prvnáček.getValue());
        druháček.setValue("Změna ve druháčkovi");
        System.out.println("Prvnáček po změně druháčka: " + prvnáček.getValue());

        /*
        Test for intercepting of logic on method level.
         */
        final IC intercepted = container2.getProxyInstance(C.class, IC.class);
        final String str = intercepted.getStr();
        System.out.println("Intercepted: " + str);
    }
}
